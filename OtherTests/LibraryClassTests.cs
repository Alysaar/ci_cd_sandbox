using MyConsoleApp;
using Xunit;

namespace OtherTests
{
    public class LibraryClassTests
    {
        [Fact]
        public void Other_Test_That_Passes()
        {
            var lib = new MyLibraryClass();
            var result = lib.Other_Method_With_UnitTests();
            Assert.Equal(42, result);
        }
    }
}