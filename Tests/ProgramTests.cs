﻿using System;
using System.IO;
using MyConsoleApp;
using Xunit;

namespace Tests
{
    public class ProgramTests : IDisposable
    {
        private readonly TextWriter _defaultConsoleOut;
        private static readonly string ExpectedOutput = "Hello World!" + Environment.NewLine +
                                                        "42" + Environment.NewLine +
                                                        "Goodbye World!" + Environment.NewLine;
        private StringWriter TestConsoleOut { get; }
            
        // Setup
        public ProgramTests()
        {
            _defaultConsoleOut = Console.Out;
            
            TestConsoleOut = new StringWriter();
            Console.SetOut(TestConsoleOut);
        }

        // Teardown
        void IDisposable.Dispose()
        {
            Console.SetOut(_defaultConsoleOut);
        }
        
        [Fact]
        public void MainTests()
        {
            Program.Main();
            
            var expectedText = ExpectedOutput.Replace("\r\n", "\n");
            var actualText = TestConsoleOut.ToString().Replace("\r\n", "\n");
            
            Assert.Equal(expectedText, actualText);
        }
    }
}