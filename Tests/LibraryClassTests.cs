using MyConsoleApp;
using Xunit;

namespace Tests
{
    public class LibraryClassTests
    {
        [Fact]
        public void Test_That_Passes()
        {
            var lib = new MyLibraryClass();
            var result = lib.Method_With_UnitTests();
            Assert.Equal(42, result);
        }
        
        [Fact]
        public void Test_That_Passes_2()
        {
            var lib = new MyLibraryClass();
            var result = lib.Method_With_UnitTests();
            Assert.Equal(42, result);
        }
        
        // [Fact]
        // public void Test_That_Fails()
        // {
        //     var lib = new MyLibraryClass();
        //     var result = lib.Method_With_UnitTests();
        //     Assert.Equal(0, result);
        // }
    }
}