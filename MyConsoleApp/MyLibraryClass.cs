﻿namespace MyConsoleApp
{
    public class MyLibraryClass
    {
        public int Method_With_UnitTests()
        {
            return 42;
        }
        public int Other_Method_With_UnitTests()
        {
            return 42;
        }
        
        public int Method_Without_UnitTests()
        {
            return 42;
        }
    }
}