﻿using System;

namespace MyConsoleApp
{
    public class Program
    {
        public static void Main()
        {
            var mylib = new MyLibraryClass();
            Console.WriteLine("Hello World!");
            Console.WriteLine(mylib.Method_With_UnitTests());
            Console.WriteLine("Goodbye World!");
        }
    }
}